package sortersTesterClasses;
import sortersTesterClasses.Entero;
import sorterClasses.InsertionSortSorter;


public class EnteroTester {

	public static void main(String[] args) {
		Entero[] EntArray = new Entero[6];
		EntArray[0] = new Entero(5);
		EntArray[1] = new Entero(13);
		EntArray[2] = new Entero(2);
		EntArray[3] = new Entero(8);
		EntArray[4] = new Entero(25);
		EntArray[5] = new Entero(0);
		
		InsertionSortSorter<Entero> sort = new InsertionSortSorter<Entero>();
		
		sort.sort(EntArray, null);
		
		for (int i = 0; i < EntArray.length; i++) {
			System.out.println(EntArray[i]);
		}
	}

}
