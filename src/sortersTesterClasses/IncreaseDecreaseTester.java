package sortersTesterClasses;
import sorterClasses.InsertionSortSorter;
import sortersTesterClasses.IntegerComparator1;
import sortersTesterClasses.IntegerComparator2;
public class IncreaseDecreaseTester {

	public static void main(String[] args) {
		InsertionSortSorter<Integer> sort = new InsertionSortSorter<Integer>();
		IntegerComparator1 inc = new IntegerComparator1();
		IntegerComparator2 dec = new IntegerComparator2();
		Integer[] a = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
		
		sort.sort(a, inc);
		System.out.println("Increasing:");
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
		sort.sort(a, dec);
		System.out.println("Decreasing:");
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
		
	}

}
